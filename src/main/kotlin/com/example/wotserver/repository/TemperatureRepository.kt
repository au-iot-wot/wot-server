package com.example.wotserver.repository

import com.example.wotserver.model.TemperatureRecord
import org.springframework.data.repository.Repository
import java.time.LocalDateTime

interface TemperatureRepository: Repository<TemperatureRecord, Long> {
    fun findTemperatureRecordsByObservationTimeBetween(startDate: LocalDateTime, endDate: LocalDateTime):List<TemperatureRecord>
    fun findAll():List<TemperatureRecord>
    fun findAllByDeviceIdentifier(id: String):List<TemperatureRecord>
    fun save(pressure: TemperatureRecord)
}