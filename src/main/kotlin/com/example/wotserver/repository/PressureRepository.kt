package com.example.wotserver.repository

import com.example.wotserver.model.PressureRecord
import org.springframework.data.repository.Repository
import java.time.LocalDateTime

interface PressureRepository: Repository<PressureRecord, Long>{

    fun findPressureRecordsByObservationTimeBetween(startDate:LocalDateTime, endDate: LocalDateTime):List<PressureRecord>
    fun findAll():List<PressureRecord>
    fun findAllByDeviceIdentifier(id: String):List<PressureRecord>
    fun save(pressure: PressureRecord)
}