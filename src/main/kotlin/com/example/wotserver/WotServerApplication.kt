package com.example.wotserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WotServerApplication

fun main(args: Array<String>) {
    runApplication<WotServerApplication>(*args)
}
