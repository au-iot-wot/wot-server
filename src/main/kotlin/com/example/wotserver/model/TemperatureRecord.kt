package com.example.wotserver.model

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity(name = "temperature")
data class TemperatureRecord(
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Int?,
    var observationTime: LocalDateTime?,
    val temperature:Double,
    var deviceIdentifier:String,
    )
