package com.example.wotserver.listener

import com.example.wotserver.model.PressureRecord
import com.example.wotserver.model.TemperatureRecord
import com.example.wotserver.repository.PressureRepository
import com.example.wotserver.repository.TemperatureRepository
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component
import java.time.LocalDateTime


@Component
class WeatherListener(val temperatureRepository: TemperatureRepository,
                      val pressureRepository: PressureRepository) {

    private val logger = LoggerFactory.getLogger(javaClass)

    @RabbitListener(queues = ["temperature"])
    fun receiveTemperature(tempJson:String){;
        val temperature=jacksonObjectMapper().readValue(tempJson, TemperatureRecord::class.java)
        temperature.observationTime= LocalDateTime.now()
        logger.info("Received temperature reading of $temperature")
        temperatureRepository.save(temperature)
    }

    @RabbitListener(queues = ["pressure"])
    fun receivePressure(pressureJson:String){
        val pressure=jacksonObjectMapper().readValue(pressureJson, PressureRecord::class.java)
        pressure.observationTime= LocalDateTime.now();
        logger.info("Received pressure reading of $pressure")
        pressureRepository.save(pressure)
    }
}