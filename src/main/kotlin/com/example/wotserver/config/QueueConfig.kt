package com.example.wotserver.config

import org.springframework.amqp.core.Queue
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class QueueConfig {

    @Bean
    fun temperatureQueue(): Queue{
        return Queue("temperature")
    }

    @Bean
    fun pressureQueue(): Queue{
        return Queue("pressure")
    }

}