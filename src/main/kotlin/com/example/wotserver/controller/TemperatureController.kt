package com.example.wotserver.controller

import com.example.wotserver.model.TemperatureRecord
import com.example.wotserver.repository.TemperatureRepository
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.*
import java.time.LocalDate

@RestController
class TemperatureController(val temperatureRepository: TemperatureRepository) {


    @RequestMapping(value = ["/temperature/"], params = ["startDate", "endDate"], method = [RequestMethod.GET])
    fun getTemperatureByDate(@RequestParam("startDate") @DateTimeFormat(pattern="yyyy-MM-dd")  startDate: LocalDate, @RequestParam("endDate") @DateTimeFormat(pattern="yyyy-MM-dd") endDate: LocalDate): List<TemperatureRecord>{
        return temperatureRepository.findTemperatureRecordsByObservationTimeBetween(startDate.atStartOfDay(), endDate.atTime(23, 59))
    }

    @GetMapping("/{deviceId}/temperature/")
    fun getTemperatureByDevice(@PathVariable deviceId:String): List<TemperatureRecord>{
        return temperatureRepository.findAllByDeviceIdentifier(deviceId).toList()
    }

    @GetMapping("/temperature/all")
    fun getAllTemperatureRecords(): List<TemperatureRecord>{
        return temperatureRepository.findAll().toList()
    }

}