package com.example.wotserver.controller

import com.example.wotserver.model.PressureRecord
import com.example.wotserver.repository.PressureRepository
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.*
import java.time.LocalDate

@RestController
class PressureController(val pressureRepository: PressureRepository) {

    @GetMapping("/pressure/all")
    fun getAllPressureRecords(): List<PressureRecord>{
        return pressureRepository.findAll().toList()
    }

    @RequestMapping(value = ["/pressure/"], params = ["startDate", "endDate"], method = [RequestMethod.GET])
    fun getPressureByDate(@RequestParam("startDate") @DateTimeFormat(pattern="yyyy-MM-dd")  startDate: LocalDate, @RequestParam("endDate") @DateTimeFormat(pattern="yyyy-MM-dd") endDate: LocalDate): List<PressureRecord>{
        return pressureRepository.findPressureRecordsByObservationTimeBetween(startDate.atStartOfDay(), endDate.atTime(23, 59))
    }

    @GetMapping("/{deviceId}/pressure/")
    fun getPressureByDevice(@PathVariable deviceId:String): List<PressureRecord>{
        return pressureRepository.findAllByDeviceIdentifier(deviceId).toList()
    }

}